﻿class CustomerQueue
{
    Customer[] customers;
    int topCustomer = -1;
    int botCustomer = -1;
    int maxCustomerQueue;

    public CustomerQueue(int maxCustomerQueue)
    {
        this.maxCustomerQueue = maxCustomerQueue;
        customers = new Customer[maxCustomerQueue];
    }

    public bool IsFull() => botCustomer - topCustomer + 1 >= maxCustomerQueue;

    public bool IsEmpty() => topCustomer == -1 && botCustomer == -1;

    public void EnQueue(Customer customer)
    {
        if (IsFull())
        {
            throw new Exception("Customer queue is full");
        }
        if (IsEmpty())
        {
            topCustomer = 0;
            botCustomer = 0;
        }
        else
        {
            botCustomer++;
        }
        customers[botCustomer] = customer;
    }

    public Customer DeQueue()
    {
        if (IsEmpty())
        {
            throw new Exception("Customer queue is empty");
        }
        Customer customerAtTop = customers[topCustomer];
        if (topCustomer == botCustomer)
        {
            topCustomer = -1;
            botCustomer = -1;
        }
        else
        {
            topCustomer++;
        }
        return customerAtTop;
    }

    public Customer Peek()
    {
        if (IsEmpty())
        {
            throw new Exception("Customer queue is empty");
        }
        return customers[topCustomer];
    }

    public int Count => IsEmpty() ? 0 : (botCustomer - topCustomer + 1);
}
