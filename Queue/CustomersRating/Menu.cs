﻿class Menu
{
    private readonly string[] _menuOptions = new string[]
    {
        "Add customer to queue",
        "Remove customer from queue",
        "View first customer in queue",
        "Count all customers in queue",
        "Exit"
    };

    public int Display()
    {
        for (int i = 0; i < _menuOptions.Length; i++)
        {
            Console.WriteLine($"{i + 1}. {_menuOptions[i]}");
        }
        Console.Write("Enter your choice: ");
        int choice = int.Parse(Console.ReadLine());
        Console.WriteLine();
        return choice;
    }
}
