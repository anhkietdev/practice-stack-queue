﻿class Program
{
    static void Main(string[] args)
    {
        CustomerQueue customerQueue = new CustomerQueue(10);
        Menu menu = new Menu();
        try
        {
            while (true)
            {
                int choice = menu.Display();
                switch (choice)
                {
                    case 1:
                        Console.Write("Enter customer name: ");
                        string name = Console.ReadLine();
                        Console.Write("Enter customer age: ");
                        int age = int.Parse(Console.ReadLine());
                        Console.Write("Enter customer address: ");
                        string address = Console.ReadLine();
                        Console.WriteLine();
                        Customer customer = new Customer { Name = name, Age = age, Address = address };
                        customerQueue.EnQueue(customer);
                        break;
                    case 2:
                        Customer removedCustomer = customerQueue.DeQueue();
                        Console.WriteLine($"Removed customer {removedCustomer.Name} from queue");
                        break;
                    case 3:
                        Customer nextCustomer = customerQueue.Peek();
                        Console.WriteLine($"First customer in queue is {nextCustomer.Name}");
                        break;
                    case 4:
                        int countCustomer = customerQueue.Count;
                        Console.WriteLine($"There are {countCustomer} customer in queue");
                        break;
                    case 5:
                        return;
                    default:
                        Console.WriteLine("Invalid choice");
                        break;
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        
    }
}
