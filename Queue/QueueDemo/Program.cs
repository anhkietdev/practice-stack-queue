﻿class Program
{
    static void Main(string[] args)
    {
        try
        {
            DemoQueue queue = new DemoQueue(5);

            queue.EnQueue(1);
            queue.EnQueue(2);
            queue.EnQueue(3);
            queue.EnQueue(4);
            queue.EnQueue(5);
            queue.EnQueue(6);

            Console.WriteLine("Dequeue: " + queue.Dequeue());
            Console.WriteLine("Dequeue: " + queue.Dequeue());
            Console.WriteLine("Dequeue: " + queue.Dequeue());
            Console.WriteLine("Dequeue: " + queue.Dequeue());
            Console.WriteLine("Dequeue: " + queue.Dequeue());


            Console.WriteLine("Peek: " + queue.Peek());
            Console.WriteLine("Count: " + queue.Count);
        }
        catch (Exception ex)
        {

            Console.WriteLine(ex.Message);
        }
    }
}