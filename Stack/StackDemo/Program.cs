﻿class Program
{
    static void Main(string[] args)
    {
        Stack stack = new Stack(10);

        stack.Push(1);
        stack.Push(2);
        stack.Push(3);
        stack.Push(4);
        stack.Push(5);

        Console.WriteLine(stack.Pop());
        Console.WriteLine(stack.Pop());
        Console.WriteLine(stack.Pop());

        Console.WriteLine(stack.Peek());
        Console.WriteLine(stack.Count);
    }
}