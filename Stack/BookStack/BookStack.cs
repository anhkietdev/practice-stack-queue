﻿class BookStack
{
    Book[] books;
    public int topBook = -1;
    public int maxBookStackSize;

    public BookStack(int maxBookStackSize)
    {
        this.maxBookStackSize = maxBookStackSize;
        books = new Book[maxBookStackSize];
    }

    public bool IsEmpty() => topBook == -1;

    public bool IsFull() => topBook == maxBookStackSize - 1;

    public void Push(Book book)
    {
        if (IsFull())
        {
            throw new Exception("Book stack is full");
        }
        topBook++;
        books[topBook] = book;
    }

    public Book Pop()
    {
        if (IsEmpty())
        {
            throw new Exception("Book tack is empty");
        }
        Book topBookValue = books[topBook];
        topBook--;
        return topBookValue;
    }

    public Book Peek()
    {
        if (IsEmpty())
        {
            throw new Exception("Book stack is empty");
        }
        return books[topBook];
    }

    public int Count => topBook + 1;
}
