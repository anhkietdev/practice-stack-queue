﻿class Menu
{
    private readonly string[] _menuOptions = new string[]
    {
        "Add a book to stack",
        "Remove a book from stack",
        "View top book information",
        "Count all books",
        "Exit"
    };

    public int Display()
    {
        for (int i = 0; i < _menuOptions.Length; i++)
        {
            Console.WriteLine($"{i + 1}. {_menuOptions[i]}");
        }
        Console.Write("Enter your choice: ");
        int choice = int.Parse(Console.ReadLine());
        Console.WriteLine();
        return choice;
    }
}


//If we hardcore content of menu options, whenever we wanna add, remove or modify this, We have to 
//change in many lines of code
//Instead of that, storing menu options in a string array help us modify functions easier. For example when
//we just add one string to array to declare this function.