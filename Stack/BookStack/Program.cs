﻿class Program
{
    static void Main(string[] args)
    {
        BookStack bookStack = new BookStack(10);
        Menu menu = new Menu();
        try
        {
            while (true)
            {
                int choice = menu.Display();
                switch (choice)
                {
                    case 1:
                        Console.Write("Enter Book ID: ");
                        int id = int.Parse(Console.ReadLine());
                        Console.Write("Enter Book title: ");
                        string title = Console.ReadLine();
                        Console.Write("Enter book description: ");
                        string description = Console.ReadLine();
                        Console.WriteLine();
                        Book book = new Book { Id = id, Title = title, Description = description };
                        bookStack.Push(book);
                        break;
                    case 2:
                        Book removedBook = bookStack.Pop();
                        Console.WriteLine($"Removed book {removedBook.Title} from stack");
                        break;
                    case 3:
                        Book firstBook = bookStack.Peek();
                        Console.WriteLine($"First book in stack is {firstBook.Title}");
                        break;
                    case 4:
                        int countAllBooks = bookStack.Count;
                        Console.WriteLine($"There are {countAllBooks} in stack");
                        break;
                    case 5:
                        return;
                    default:
                        Console.WriteLine("Invalid choice");
                        break;
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
}